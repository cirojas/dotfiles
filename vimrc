" vim plug: https://github.com/junegunn/vim-plug
call plug#begin('~/.vim/plugged')
Plug 'scrooloose/nerdtree'
Plug 'Xuyuanp/nerdtree-git-plugin'
Plug 'bronson/vim-trailing-whitespace'
call plug#end()

syntax on
set background=dark
" colorscheme jellybeans

if has("autocmd")
	filetype plugin indent on
endif

set autoindent

" line number
set number
set relativenumber

" indenting
set tabstop=4
set shiftwidth=4
set expandtab

let mapleader=" "

" NERDTree
map <Leader>n :NERDTreeToggle<CR>
" close if only window open is NERDTree
autocmd bufenter * if (winnr("$") == 1 && exists("b:NERDTree") && b:NERDTree.isTabTree()) | q | endif

" Window navigation
nmap <silent> <Leader><Up> :wincmd k<CR>
nmap <silent> <Leader><Down> :wincmd j<CR>
nmap <silent> <Leader><Left> :wincmd h<CR>
nmap <silent> <Leader><Right> :wincmd l<CR>
