# Path to your oh-my-zsh installation.
export ZSH=$HOME/.oh-my-zsh

ZSH_THEME="clean"
export UPDATE_ZSH_DAYS=15

plugins=(
  git
)

source $ZSH/oh-my-zsh.sh
source $HOME/.rvm/scripts/rvm

# Add RVM to PATH for scripting.
export PATH="$PATH:$HOME/.rvm/bin"

# Preferred editor for local and remote sessions
export EDITOR='vim'

